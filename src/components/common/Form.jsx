import React, { Component } from "react";
import Joi from "joi-browser";

class Form extends Component {
  state = {
    data: {},
    error: null
  };

  handleSubmit = async e => {
    e.preventDefault();

    this.onSubmit(e);
  };

  handleChange = e => {
    const { name, value } = e.currentTarget;
    const state = { ...this.state };
    state.data[name] = value;
    this.setState(state);
    this.validate();
  };

  validate = () => {
    const { error } = Joi.validate(this.state.data, this.schema);

    this.setState({ error });
  };

  renderInput = (name, id, placeholder, type = "text") => {
    return (
      <div className="form-group">
        <label htmlFor={id}>{id}</label>
        <input
          className="form-control"
          type={type}
          name={name}
          id={id}
          placeholder={placeholder}
          onChange={e => this.handleChange(e)}
        />
      </div>
    );
  };
}

export default Form;
