import React from "react";
import Form from "./common/Form";
import Joi from "joi-browser";
import auth from "../services/authService";

class Register extends Form {
  state = {
    data: {
      name: "",
      email: "",
      password: ""
    },
    error: null
  };

  schema = Joi.object().keys({
    name: Joi.string()
      .required()
      .label("Name"),
    email: Joi.string()
      .email({ minDomainSegments: 2 })
      .required()
      .label("Email"),
    password: Joi.string()
      .alphanum()
      .min(8)
      .max(30)
      .required()
      .label("Password")
  });

  onSubmit = async e => {
    const { name, email, password } = e.currentTarget;

    this.validate();

    if (!this.state.error) {
      const { data } = await auth.register(
        name.value,
        email.value,
        password.value
      );

      if (data && data.jwt) auth.setJwt(data.jwt);

      window.location = "/";
    }

    return;
  };

  render() {
    const { error } = this.state;

    if (auth.getCurrentUser()) window.location = "/";

    return (
      <form className="w-50 mx-auto" onSubmit={e => this.handleSubmit(e)}>
        <h2>Register Form</h2>
        {this.renderInput("name", "Name", "Enter name")}
        {this.renderInput("email", "Email", "Enter email address", "email")}
        {this.renderInput("password", "Password", "Enter password", "password")}
        {error && (
          <div className="alert alert-danger">{error.details[0].message}</div>
        )}
        <button
          disabled={error}
          type="submit"
          className="btn btn-primary w-100"
        >
          Submit
        </button>
      </form>
    );
  }
}

export default Register;
