import React from "react";
import Joi from "joi-browser";
import Form from "./common/Form";
import auth from "../services/authService";

class Login extends Form {
  state = {
    data: {
      email: "",
      password: ""
    },
    error: null
  };

  schema = Joi.object().keys({
    email: Joi.string()
      .email({ minDomainSegments: 2 })
      .required()
      .label("Email"),
    password: Joi.string()
      .alphanum()
      .required()
      .label("Password")
  });

  onSubmit = async e => {
    const { email, password } = e.currentTarget;

    this.validate();

    if (!this.state.error) {
      const { jwt } = await auth.login(email.value, password.value);
      window.location = jwt ? "/" : ".";
    }
  };

  render() {
    const { error } = this.state;

    if (auth.getCurrentUser()) window.location = "/";

    return (
      <form className="w-50 mx-auto" onSubmit={e => this.handleSubmit(e)}>
        <h2>Login Form</h2>
        {this.renderInput("email", "Email", "Enter email address", "email")}
        {this.renderInput("password", "Password", "Enter password", "password")}
        {error && (
          <div className="alert alert-danger">{error.details[0].message}</div>
        )}
        <button
          disabled={error}
          type="submit"
          className="btn btn-primary w-100"
        >
          Submit
        </button>
      </form>
    );
  }
}

export default Login;
