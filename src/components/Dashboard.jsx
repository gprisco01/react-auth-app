import React, { Component } from "react";
import auth from "../services/authService";

class Dashboard extends Component {
  state = {
    user: {},
    data: undefined
  };

  async componentDidMount() {
    const user = auth.getCurrentUser();

    if (user && user.isAdmin) {
      const { data } = await auth.getUsers();
      this.setState({ data });
    }

    this.setState({ user });
  }

  render() {
    const { user, data } = this.state;

    const handleDelete = async e => {
      const { id } = e.currentTarget;
      const users = [...this.state.data];

      const { data } = await auth.deleteUser(id);

      users.forEach(user => {
        if (user._id === data._id) users.splice(users.indexOf(user), 1);
      });

      this.setState({ data: users });
    };

    const adminDash = () => {
      if (data.error) return <h5>{data.error}</h5>;

      return (
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col"></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {data.map(user => {
              return (
                <tr key={user._id}>
                  <th scope="row">{user._id}</th>
                  <td>{user.name}</td>
                  <td>{user.email}</td>
                  <td>{user.isAdmin && "Admin"}</td>
                  <td>
                    <button
                      id={user._id}
                      className="btn btn-danger btn-sm"
                      onClick={e => handleDelete(e)}
                      disabled={user.isAdmin}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
    };

    return (
      <div className="container justify-content-center">
        {user && <h1>Welcome {user.name}</h1>}
        {user && user.isAdmin && <h4>You are an admin</h4>}
        {!user && <h1>Please Login or Register</h1>}
        {data && adminDash()}
      </div>
    );
  }
}

export default Dashboard;
