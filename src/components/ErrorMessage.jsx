import React from 'react';

const ErrorMessage = (props) => {
  return (
    <h5 className="justify-content-center">{ props.state.data }</h5>
  );
}
 
export default ErrorMessage;