import axios from "axios";
import qs from "qs";
import jwtDecode from "jwt-decode";
import api from "../config/api.json";

const setJwt = jwt => {
  localStorage.setItem("json-web-token", jwt);
};

const getJwt = () => {
  return localStorage.getItem("json-web-token");
};

const getCurrentUser = () => {
  const jwt = getJwt();
  const user = jwt ? jwtDecode(jwt) : null;
  return user;
};

const register = async (name, email, password) => {
  const data = { name, email, password };

  const options = {
    method: "POST",
    headers: { "content-type": "application/x-www-form-urlencoded" },
    data: qs.stringify(data),
    url: api.endpoint + "/register"
  };

  let res = await axios(options);

  if (res.status === 200) {
    res = await login(email, password);
  }

  return res;
};

const login = async (email, password) => {
  const payload = { email, password };

  const options = {
    method: "POST",
    headers: { "content-type": "application/x-www-form-urlencoded" },
    data: qs.stringify(payload),
    url: api.endpoint + "/login"
  };

  const { data } = await axios(options);

  setJwt(data.jwt);

  return data;
};

const logout = () => {
  localStorage.removeItem("json-web-token");
};

const getUsers = async () => {
  const options = {
    method: "POST",
    headers: { "content-type": "application/x-www-form-urlencoded" },
    data: qs.stringify({ token: getJwt() }),
    url: api.endpoint + "/getUsers"
  };

  const data = await axios(options);

  return data;
};

const deleteUser = async (_id) => {
  const options = {
    method: "DELETE",
    headers: { "content-type": "application/x-www-form-urlencoded" },
    data: qs.stringify({ token: getJwt(), _id }),
    url: api.endpoint + "/deleteUser"
  };

  const data = await axios(options);

  return data;
};

export default {
  register,
  getJwt,
  setJwt,
  login,
  logout,
  getCurrentUser,
  getUsers,
  deleteUser
};
