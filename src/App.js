import React, { Component } from "react";
import NavBar from "./components/Navbar";
import Dashboard from "./components/Dashboard";
import Register from "./components/Register";
import Login from "./components/Login";
import { BrowserRouter, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

class App extends Component {
  state = {
    jwt: undefined
  };

  render() {
    return (
      <BrowserRouter>
        <NavBar />

        <Route exact path="/" component={Dashboard} />
        {this.state.jwt || (
          <React.Fragment>
            <Route path="/login" component={Login} />
            <Route path="/register" component={Register} />
          </React.Fragment>
        )}
      </BrowserRouter>
    );
  }
}

export default App;
